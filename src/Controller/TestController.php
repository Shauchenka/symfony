<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 10.06.19
 * Time: 23:10
 */

namespace App\Controller;


use App\Entity\Paswords;
use App\Entity\Test;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/api/test", name="test")
     */
    public function testAction(Request $request)
    {

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $a = new Test();
        $a->setName("taras");
        $a->setAge(1112);
        $em->persist($a);

        for($i = 1; $i< 10; $i++) {
            $b = new Paswords();
            $b->setPass("pass:" . $i);
            $b->setTest($a);
            $em->persist($b);
        }

        $em->flush();



        echo "TEST";
        return new Response('success');
    }

    /**
     * @Route("/show/{id}")
     *
     */
    public function showAction($id)
    {
        $test = $this->getDoctrine()->getRepository(Test::class)->find($id);

        /** @var Paswords $password */
        foreach ($test->getPasswords() as $password) {
            echo $password->getPass() . "<BR>";
        }

        $b = new Paswords();
        $b->setPass("dsgsdgds");

        $this->getDoctrine()->getManager(Test::class);


        die;
    }
}