<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Paswords", mappedBy="test")
     */
    private $passwords;

    /**
     * Test constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->passwords = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Test
     */
    public function setName(string $name): Test
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return Test
     */
    public function setAge(int $age): Test
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPasswords(): Collection
    {
        return $this->passwords;
    }

    /**
     * @param ArrayCollection $passwords
     * @return Test
     */
    public function setPasswords(Collection $passwords): Test
    {
        $this->passwords = $passwords;
        return $this;
    }


}
