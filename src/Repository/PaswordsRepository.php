<?php

namespace App\Repository;

use App\Entity\Paswords;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Paswords|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paswords|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paswords[]    findAll()
 * @method Paswords[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaswordsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Paswords::class);
    }

    // /**
    //  * @return Paswords[] Returns an array of Paswords objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Paswords
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
